# LibSocket

## Como Usar

    private void createTerminal(int alPort)
    {
        var terminal = new ServerTerminal();
                
        terminal.MessageRecived += new MessageRecived(m_Terminal_MessageRecived);
        terminal.ClientConnect += new Connect(m_Terminal_ClientConnected);
        terminal.ClientDisconnect += new Disconnect(m_Terminal_ClientDisConnected);

        terminal.StartListen(alPort, Protocol.TCP);
    }


    void m_Terminal_ClientDisConnected(Socket socket)
    {
        // Cliente Desconectado
    }

    void m_Terminal_ClientConnected(Socket socket)
    {
        // Cliente Conectado
    }

    void m_Terminal_MessageRecived(Socket socket, byte[] buffer)
    {
        string message = ConvertBytesToString(buffer, buffer.Length);
        // Cliente Recebido
    }