using System;
using System.Net.Sockets;

namespace LibSockets
{
    public delegate void MessageRecived(Socket socket, byte[] message, int length);
    public delegate void Connect(Socket socket);
    public delegate void Disconnect(Socket socket);
    public delegate void Error(Exception ex);
    public delegate void ErrorConnection(Exception ex);
}
