using System;
using System.Net.Sockets;
using System.Threading;

namespace LibSockets.Server
{
    public class ConnectedClient
    {

        public event Error Error;
        public event Disconnect ClientClose;

        private Socket m_clientSocket;
        SocketListener m_listener;

        int _timeOutClient = 0;

        public bool Connected { get; set; }

        public int TimeOutClient
        {
            get { return _timeOutClient; }
            set { _timeOutClient = value; }
        }

        public event MessageRecived MessageRecived
        {
            add
            {
                m_listener.MessageRecived += value;
            }
            remove
            {
                m_listener.MessageRecived -= value;
            }
        }
        public event Disconnect Disconnected
        {
            add
            {
                m_listener.Disconnected += value;
            }
            remove
            {
                m_listener.Disconnected -= value;
            }
        }

        public ConnectedClient(Socket clientSocket)
        {
            try
            {
                m_clientSocket = clientSocket;
                m_listener = new SocketListener();
            }
            catch (Exception e)
            {
                if (Error != null)
                {
                    Error(new Exception(e.Message));
                }
            }
        }

        public void StartListen()
        {
            new Thread(StartListen2).Start();
        }


        private void StartListen2()
        {
            m_listener.StartReciving(m_clientSocket, TimeOutClient);
        }

        public void Send(byte[] buffer)
        {
            bool close = false;
            try
            {
                if (m_clientSocket == null)
                {
                    if (Error != null)
                    {
                        Error(new Exception("Can't send data. ConnectedClient is Closed!"));
                    }

                    if (ClientClose != null)
                    {
                        ClientClose(m_clientSocket);
                    }
                    close = true;
                }
                else
                {
                    m_clientSocket.NoDelay = true;
                    m_clientSocket.SendBufferSize = buffer.Length;
                    m_clientSocket.Send(buffer, buffer.Length, SocketFlags.None);
                }
            }
            catch (Exception e)
            {
                close = true;
                if (ClientClose != null)
                {
                    ClientClose(m_clientSocket);
                }
                if (Error != null)
                {
                    Error(new Exception(e.Message));
                }
            }

            if (close)
            {
                try
                {
                    m_clientSocket.Close();
                }
                catch { }
            }
        }

        public void Stop()
        {
            try
            {
                m_listener.StopListening();
                if (ClientClose != null)
                {
                    ClientClose(m_clientSocket);
                }
                m_clientSocket = null;
            }
            catch (Exception)
            {
                //if (Error != null)
                //{
                //    Error(new Exception(e.Message));
                //}
            }
        }
    }
}