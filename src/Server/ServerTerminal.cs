using LibSockets.Server;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace LibSockets.Server
{
    public class ServerTerminal
    {
        public event MessageRecived MessageRecived;
        public event Connect ClientConnect;
        public event Disconnect ClientDisconnect;
        public event Error Error;
        public event ErrorConnection ErrorConnection;

        private int _timeoutCliente = 0;

        private Socket m_socket;
        private bool m_Closed;

        private Protocol _protocol;

        private Dictionary<string, ConnectedClient> m_clients = new Dictionary<string, ConnectedClient>();
        
        public void StartListen(int port, Protocol protocol = Protocol.TCP)
        {
            try
            {

                _protocol = protocol;

                IPEndPoint ipLocal = new IPEndPoint(IPAddress.Any, port);

                switch (_protocol)
                {
                    case Protocol.TCP:
                        m_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                        break;
                    case Protocol.UDP:
                        m_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                        break;
                }
                
                //bind to local IP Address...
                //if ip address is allready being used write to log

                m_socket.Bind(ipLocal);

                //start listening...
                m_socket.Listen((int)SocketOptionName.MaxConnections);

                // create the call back for any client connections...
                m_socket.BeginAccept(new AsyncCallback(OnClientConnection), null);

            }
            catch (Exception)
            {
                if (ErrorConnection != null)
                {
                    ErrorConnection(new Exception(string.Format("Can't connect to port {0}!", port)));
                }
            }
                        
        }

        public int TimeOutClient
        {
            get { return _timeoutCliente; }
            set { _timeoutCliente = value; }
        }

        public int ReceiverTimeOut
        {
            get { return m_socket.ReceiveTimeout; }
            set { m_socket.ReceiveTimeout = value; }
        }

        public int SendTimeOut
        {
            get { return m_socket.SendTimeout; }
            set { m_socket.SendTimeout = value; }
        }

        private void OnClientConnection(IAsyncResult asyn)
        {
            if (m_Closed)
            {
                return;
            }

            try
            {
                Socket clientSocket = m_socket.EndAccept(asyn);

                RaiseClientConnected(clientSocket);
                                
                ConnectedClient connectedClient = new ConnectedClient(clientSocket);
                connectedClient.TimeOutClient = _timeoutCliente;
                connectedClient.Connected = true;

                connectedClient.MessageRecived += OnMessageRecived;
                connectedClient.Disconnected += OnClientDisconnection;
                connectedClient.ClientClose += OnClientDisconnection;

                connectedClient.StartListen();

                long key = clientSocket.Handle.ToInt64();
                if (m_clients.ContainsKey(key.ToString()))
                {
                    if (Error != null)
                    {
                        Error(new Exception(string.Format("Client with handle key '{0}' already exist!", key)));
                    }
                }

                m_clients[key.ToString()] = connectedClient;

                // create the call back for any client connections...
                m_socket.BeginAccept(new AsyncCallback(OnClientConnection), null);

            }
            catch (ObjectDisposedException)
            {
                if (Error != null)
                {
                    Error(new Exception("ServerTerminal.OnClientConnection: Socket has been closed"));
                }
            }
            catch (Exception)
            {
                if (Error != null)
                {
                    Error(new Exception("ServerTerminal.OnClientConnection: Socket failed"));
                    //Console.WriteLine(sex.Message + " - " + sex.StackTrace);
                }
                m_socket.BeginAccept(new AsyncCallback(OnClientConnection), null);
            }

        }

        private void OnClientDisconnection(Socket socket)
        {
            try
            {

                RaiseClientDisconnected(socket);

                long key = socket.Handle.ToInt64();
                if (m_clients.ContainsKey(key.ToString()))
                {
                    m_clients.Remove(key.ToString());
                }
                else
                {
                    if (Error != null)
                    {
                        Error(new Exception(string.Format("Unknown client '{0}' has been disconncoted!", key)));
                    }
                }
            }
            catch (Exception ex)
            {
                if (Error != null)
                {
                    Error(new Exception(ex.Message));
                }
            }
        }

        private static byte[] StrToByteArray(string str)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return encoding.GetBytes(str);
        }

        public void DistributeMessage(string buffer)
        {
            try
            {
                DistributeMessage(StrToByteArray(buffer));
            }
            catch (SocketException se)
            {
                throw se;
            }
        }

        public void DistributeMessage(byte[] buffer)
        {
            // Display them.

            List<string> keys = new List<string>(m_clients.Keys);
            foreach(string key in keys)

            //foreach (var item in m_clients.T)
            //foreach (var value in m_clients)
            {
                ConnectedClient c = m_clients[key];
                try
                {
                    //SendDistribute(m_clients[value], buffer);
                    //m_clients[value].Send(buffer);

                    SendDistribute(new DataSendDistribuite()
                    {
                        Client = c,
                        MyBuffer = buffer
                    });

                    /*
                    Thread t = new Thread(new ParameterizedThreadStart(SendDistribute));
                    t.Start(
                        new DataSendDistribuite()
                        {
                            Client = m_clients[value],
                            MyBuffer = buffer
                        }
                    );*/
                }
                catch (Exception)
                {
                    CloseConection(c);
                    m_clients.Remove(key);
                }
            }
        }

        private void SendDistribute(object o)
        {
            try
            {
                DataSendDistribuite d = (DataSendDistribuite)o;
                d.Client.Send(d.MyBuffer);
                //conn.Send(buffer);
            }
            catch (Exception se)
            {
                throw se;
            }
        }

        private void CloseConection( ConnectedClient conn )
        {
            try
            {
                conn.Stop();
            }
            catch { }
        }


        public void Close()
        {
            try
            {
                if (m_socket != null)
                {
                    m_Closed = true;

                    // Close the clients
                    foreach (ConnectedClient connectedClient in m_clients.Values)
                    {
                        connectedClient.Stop();
                    }
                    m_socket.Close();
                    m_socket = null;
                }
            }
            catch (ObjectDisposedException)
            {
                if (Error != null)
                {
                    Error(new Exception("Stop failed"));
                }
            }
        }

        private void OnMessageRecived(Socket socket, byte[] message, int length)
        {
            if (MessageRecived != null)
            {
                MessageRecived(socket, message, length);
            }
        }

        private void RaiseClientConnected(Socket socket)
        {
            if (ClientConnect != null)
            {
                ClientConnect(socket);
            }
        }

        private void RaiseClientDisconnected(Socket socket)
        {
            if (ClientDisconnect != null)
            {
                ClientDisconnect(socket);
            }
        }
    }
}

public class DataSendDistribuite
{

    public ConnectedClient Client { get; set; }
    public byte[] MyBuffer { get; set; }

}