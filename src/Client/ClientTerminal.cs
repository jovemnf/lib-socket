using System;
using System.Net;
using System.Net.Sockets;

namespace LibSockets.Client
{
    public class ClientTerminal
    {
        Socket m_socClient;
        private SocketListener m_listener;

        public event MessageRecived MessageRecived;
        public event Connect Connected;
        public event Disconnect Disconncted;
        public event Error Error;

        public void Connect(IPAddress remoteIPAddress, int alPort)
        {
            try
            {
                m_socClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                
                // Connect
                m_socClient.Connect(new IPEndPoint(remoteIPAddress, alPort));

                OnServerConnection();
            }
            catch (Exception ex)
            {
                if (Error != null)
                {
                    Error(new Exception(ex.Message));
                }
            }
        }
        
        public void SendMessage(string message, int timer = 5000)
        {
            try
            {
                byte[] byData = System.Text.Encoding.ASCII.GetBytes(message + System.Environment.NewLine);
                this.SendMessage(byData, timer);
            }
            catch (SocketException soc)
            {
                throw soc;
            }
            catch (ObjectDisposedException soc)
            {
                throw soc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendMessage(byte[] byData, int timer = 5000)
        {
            try
            {
                m_socClient.ReceiveTimeout = timer;
                m_socClient.Send(byData, 0, byData.Length, SocketFlags.None);
            }
            catch (SocketException soc)
            {
                throw soc;
            }
            catch (ObjectDisposedException soc)
            {
                throw soc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void StartListen()
        {
            try
            {
                if (m_socClient == null)
                {
                    return;
                }

                if (m_listener != null)
                {
                    return;
                }

                m_listener = new SocketListener();
                m_listener.Disconnected += OnServerConnectionDroped;
                m_listener.MessageRecived += OnMessageRecived;
                m_listener.StartReciving(m_socClient);
            }
            catch (Exception e)
            {
                if (Error != null)
                {
                    Error(new Exception(e.Message));
                }
            }
        }

        public string ReadData()
        {
            System.String szData = "";
            try
            {
                if (m_socClient == null)
                {
                    return string.Empty;
                }

                byte[] buffer = new byte[1024];
                int iRx = m_socClient.Receive(buffer);
                char[] chars = new char[iRx];

                System.Text.Decoder d = System.Text.Encoding.UTF8.GetDecoder();
                d.GetChars(buffer, 0, iRx, chars, 0);
                szData = new System.String(chars);
                
            }
            catch (Exception e)
            {
                if (Error != null)
                {
                    Error(new Exception(e.Message));
                }
            }
            return szData;
        }

        public void Close()
        {
            try
            {

                if (m_socClient == null)
                {
                    return;
                }

                if (m_listener != null)
                {
                    m_listener.StopListening();
                }

                RaiseServerDisconnected(m_socClient);

                m_socClient.Close();
                m_listener = null;
                m_socClient = null;

            }
            catch (Exception e)
            {
                if (Error != null)
                {
                    Error(new Exception(e.Message));
                }
            }
        }

        private void OnServerConnection()
        {
            try
            {
                if (Connected != null)
                {
                    Connected(m_socClient);
                }
            }
            catch (Exception e)
            {
                if (Error != null)
                {
                    Error(new Exception(e.Message));
                }
            }
        }

        private void OnMessageRecived(Socket socket, byte[] message, int length)
        {
            try
            {
                if (MessageRecived != null)
                {
                    MessageRecived(socket, message, length);
                }
            }
            catch (Exception e)
            {
                if (Error != null)
                {
                    Error(new Exception(e.Message));
                }
            }
        }

        private void OnServerConnectionDroped(Socket socket)
        {
            try
            {
                Close();
                RaiseServerDisconnected(socket);
            }
            catch (Exception e)
            {
                if (Error != null)
                {
                    Error(new Exception(e.Message));
                }
            }
        }

        private void RaiseServerDisconnected(Socket socket)
        {
            try
            {
                if (Disconncted != null)
                {
                    Disconncted(socket);
                }
            }
            catch (Exception e)
            {
                if (Error != null)
                {
                    Error(new Exception(e.Message));
                }
            }
        }

    }
}