using System;
using System.Diagnostics;
using System.Net.Sockets;

namespace LibSockets
{
    public class SocketListener
    {
        public class CSocketPacket
        {
            public Socket thisSocket;
            public byte[] dataBuffer;

            public CSocketPacket(int buffeLength)
            {
                dataBuffer = new byte[buffeLength];
            }
        }

        private const int BufferLength = 2000;
        AsyncCallback pfnWorkerCallBack;
        Socket m_socWorker;

        public event MessageRecived MessageRecived;
        public event Disconnect Disconnected;

        public event Error Error;

        //Timer
        static System.Timers.Timer tConnectTimeout;

        int _timeOutClient = 0;

        public void StartReciving(Socket socket, int timeout = 0)
        {
            try
            {
                m_socWorker = socket;
                _timeOutClient = timeout;
                WaitForData(socket);
            }
            catch (Exception e)
            {
                if (Error != null)
                {
                    Error(new Exception(e.Message));
                }
            }
        }

        public void StopListening()
        {
            try
            {
                // Incase connection has been established with remote client - 
                // Raise the OnDisconnection event.
                if (m_socWorker != null)
                {
                    // m_socWorker.Shutdown(SocketShutdown.Both);                        
                    m_socWorker.Close();
                    m_socWorker = null;
                }
            }
            catch (Exception e)
            {
                if (Error != null)
                {
                    Error(new Exception(e.Message));
                }
            }
        }

        private void WaitForData(Socket soc)
        {
            try
            {

                if (pfnWorkerCallBack == null)
                {
                    pfnWorkerCallBack = new AsyncCallback(OnDataReceived);
                }
                CSocketPacket theSocPkt = new CSocketPacket(BufferLength);
                theSocPkt.thisSocket = soc;
                
                // now start to listen for any data...
                soc.BeginReceive(theSocPkt.dataBuffer, 0, theSocPkt.dataBuffer.Length, SocketFlags.None, pfnWorkerCallBack, theSocPkt);

                if (_timeOutClient > 0)
                {
                    //Create timer and set timeout itnerval
                    tConnectTimeout = new System.Timers.Timer(_timeOutClient);

                    //Create handler for timer's Elapsed event
                    tConnectTimeout.Elapsed += new System.Timers.ElapsedEventHandler(tConnectTimeout_Elapsed);

                    //Start timer
                    tConnectTimeout.Start();
                }

            }
            catch (SocketException sex)
            {
                if (Error != null)
                {
                    Error(new Exception("SocketListener.WaitForData: Socket failed. "+ sex.StackTrace));
                }
                OnConnectionDroped(soc);
            }

        }

        void tConnectTimeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Drop();
            //The connection timed out
            Console.WriteLine("Connection timed out!");
            OnConnectionDroped(m_socWorker);
        }

        private void Drop()
        {
            try
            {
                //Close the socket
                m_socWorker.Close();
            }
            catch { }

            m_socWorker = null;

            try
            {
                //Stop and dispose timer
                tConnectTimeout.Dispose();
            }
            catch { }
        }

        private void OnDataReceived(IAsyncResult asyn)
        {

            CSocketPacket theSockId = (CSocketPacket)asyn.AsyncState;
            Socket socket = theSockId.thisSocket;

            try
            {
                tConnectTimeout.Enabled = false;
                tConnectTimeout.Close();
                tConnectTimeout = null;
                Console.WriteLine("Stop Time..");
            }
            catch { }
            
            try
            {

                if (!socket.Connected)
                {
                    return;
                }

                int iRx;
                try
                {
                    iRx = socket.EndReceive(asyn);
                }
                catch (SocketException)
                {
                    //Console.WriteLine("OnDataReceived: Apperently client has been closed and connot answer.");
                    OnConnectionDroped(socket);
                    return;
                }

                if (iRx == 0)
                {
                    // Console.WriteLine("OnDataReceived: Apperently client socket has been closed.");
                    // If client socket has been closed (but client still answers)- 
                    // EndReceive will return 0.
                    OnConnectionDroped(socket);
                    return;
                }

                byte[] bytes = theSockId.dataBuffer;
                
                RaiseMessageRecived(bytes,iRx);

                WaitForData(m_socWorker);

            }
            catch (Exception ex)
            {
                if (Error != null)
                {
                    Error(new Exception("SocketListener.OnDataReceived: Socket failed. "+ ex.StackTrace));
                }
                OnConnectionDroped(socket);
            }
        }
        
        private void RaiseMessageRecived(byte[] bytes,int len)
        {
            if (MessageRecived != null)
            {
                MessageRecived(m_socWorker, bytes, len);
            }
        }

        private void OnDisconnection(Socket socket)
        {
            if (Disconnected != null)
            {
                Disconnected(socket);
            }
        }

        private void OnConnectionDroped(Socket socket)
        {
            OnDisconnection(socket);
            Drop();
        }

    }
}